// var nav = require('./nav.js')
// var { EcosystemNav, ComponentNav } = nav

// var utils = require('./utils.js')
// var { genNav, getComponentSidebar, deepClone } = utils

module.exports = {
  title: 'BaseSDD',
  description: 'BaseSDD',
  base: '/',
  head: [
    [
      'link',
      {
        rel: 'icon',
        href: '/favicon.ico'
      }
    ]
  ],
  dest: './dist',
  themeConfig: {
    repo: 'YLQZ/devYuSheng',
    docsRepo: 'YLQZ/devYuSheng',
    docsDir: 'docs',
    editLinks: true,
    editLinkText: '帮助我改善此页面！',
    sidebarDepth: 4,
    search: true,
    searchMaxSuggestions: 20,
    sidebar: {
      '/01fsr/': [
        {
          // title: '基础',
          collapsable: false,
          children: genFSREssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/02mdp/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen2MDPEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/03opm/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen3OPMEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/04sum/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen04SUMEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/05dsr/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen05DSREssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/06gds/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen06GDSEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/07dds/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen07DDSEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/08dts/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen08DTSEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/09ddm/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen09DDMEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/10pdp/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen10PDPEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/11mdpr/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen11MDPREssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/12ttp/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen12TTPEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/13tar/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen13TAREssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/14pim/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen14PIMEssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ],
      '/15pdsr/': [
        {
          // title: '基础',
          collapsable: false,
          children: gen15PDSREssentialsSidebar()
        }
        // {
        //   title: '进阶',
        //   collapsable: false,
        //   children: genFSRAdvancedSidebar()
        // },
        // {
        //   title: '其它',
        //   collapsable: false,
        //   children: []
        // }
      ]
    },
    serviceWorker: {
      updatedPopup: {
        message: '新内容可用。',
        buttonText: '刷新'
      }
    },
    lastUpdated: 'Last Updated'
  },
  serviceWorker: true,
  locales: {
    '/': {
      lang: 'zh-CN',
      description: '软件设计文档 (全)'
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@public': './public'
      }
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: 'style-loader' // 将 JS 字符串生成为 style 节点
            },
            {
              loader: 'css-loader' // 将 CSS 转化成 CommonJS 模块
            },
            {
              loader: 'sass-loader' // 将 Sass 编译成 CSS
            }
          ]
        }
      ]
    }
  },
  ga: 'UA-109340118-1'
}

// TODO: 可行性研究报告 01fsr 对外路径配置
function genFSREssentialsSidebar(type = '') {
  const mapArr = [
    '/01fsr/'
    // '/fsr/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}

function genFSRAdvancedSidebar(type = '') {
  const mapArr = ['/fsr/advanced/advanced.md']
  return mapArr.map(i => {
    return type + i
  })
}

// TODO: 模块开发卷宗 02mdp 对外路径配置
function gen2MDPEssentialsSidebar(type = '') {
  const mapArr = [
    '/02mdp/'
    // '/2mdp/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}

// TODO: 操作手册 03opm 对外路径配置
function gen3OPMEssentialsSidebar(type = '') {
  const mapArr = [
    '/03opm/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 用户手册 04sum 对外路径配置
function gen04SUMEssentialsSidebar(type = '') {
  const mapArr = [
    '/04sum/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 软件需求说明书 05dsr 对外路径配置
function gen05DSREssentialsSidebar(type = '') {
  const mapArr = [
    '/05dsr/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 概要设计说明书 06gds 对外路径配置
function gen06GDSEssentialsSidebar(type = '') {
  const mapArr = [
    '/06gds/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 详细设计说明书 07dds 对外路径配置
function gen07DDSEssentialsSidebar(type = '') {
  const mapArr = [
    '/07dds/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 数据要求说明书 08dts 对外路径配置
function gen08DTSEssentialsSidebar(type = '') {
  const mapArr = [
    '/08dts/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}

// TODO: 数据库设计说明书 09ddm 对外路径配置
function gen09DDMEssentialsSidebar(type = '') {
  const mapArr = [
    '/09ddm/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 项目开发计划 10pdp 对外路径配置
function gen10PDPEssentialsSidebar(type = '') {
  const mapArr = [
    '/10pdp/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 开发进度月报 11mdpr 对外路径配置
function gen11MDPREssentialsSidebar(type = '') {
  const mapArr = [
    '/11mdpr/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 测试计划 12ttp 对外路径配置
function gen12TTPEssentialsSidebar(type = '') {
  const mapArr = [
    '/12ttp/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 测试分析报告 13tar 对外路径配置
function gen13TAREssentialsSidebar(type = '') {
  const mapArr = [
    '/13tar/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 项目实施说明书 14pim 对外路径配置
function gen14PIMEssentialsSidebar(type = '') {
  const mapArr = [
    '/14pim/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
// TODO: 项目开发总结报告 15pdsr 对外路径配置
function gen15PDSREssentialsSidebar(type = '') {
  const mapArr = [
    '/15pdsr/'
    // '/3opm/essentials/essentials.md'
  ]
  return mapArr.map(i => {
    return type + i
  })
}
