---

home: true
heroImage: /home.png
# title: a
# actionText: Get Started →
# actionLink: /guide/
features:
  - title: 可行性研究报告
    link: '/01fsr/'
    details: 软件项目的技术规范标准、目标必要性、需求分析、功能设计、配套、实施、培训、投资、效益等做分析评估设计的总体报告
  - title: 模块开发卷宗
    link: '/02mdp/'
    details: 详细记录软件开发过程;各阶段的时间和说明等等
  - title: 操作手册
    link: '/03opm/'
    details: 操作手册是软件安装、卸载及测试和错误处理等操作流程的行为准则
  - title: 用户手册
    link: '/04sum/'
    details: 对软件的用途、运行环境、使用过程等进行详细说明
  - title: 软件需求说明书
    link: '/05dsr/'
    details: 软件的需求、目标、受众、环境等作出规定
  - title: 概要设计说明书
    link: '/06gds/'
    details: 概要设计说明书是软件开发的总体设计，对接口、运行、模块、数据结构、系统错误、系统维护等做出相关设计的规定性文件
  - title: 详细设计说明书
    link: '/07dds/'
    details: 对软件系统的结构、功能、性能、算法、流程逻辑、接口、存储分配、注释设计、限制条件、测试计划、及尚未解决的问题作出规定和说明
  - title: 数据要求说明书
    link: '/08dts/'
    details: 数据的逻辑描述、数据的采集
  - title: 数据库设计说明书
    link: '/09ddm/'
    details: 外部设计、结构设计、运用设计等

  - title: 项目开发计划
    link: '/10pdp/'
    details: 工作内容、参与人员、产品、验收标准、交付日期、实施计划、支持条件等作出详细规划

  - title: 开发进度月报
    link: '/11mdpr/'
    details: 开发进度月报是对软件开发的进度、所处阶段、工时、经费、本月成果和下月计划做出汇报的文件
  - title: 测试计划
    link: '/12ttp/'
    details: 测试计划是对软件进行测试、计划安排、测试培训、评判准则的说明和指导性文件
  - title: 测试分析报告
    link: '/13tar/'
    details: 测试分析报告是对软件的功能、性能、功耗、压力等测试后的报告及分析文件
  - title: 文件给制实施规定的实例
    link: '/14pim/'
    details: Supports multiple dynamic skin methods
  - title: 项目开发总结报告
    link: '/15pdsr/'
    details: 实际开发结果、开发工作评价
  
footer: MIT Licensed | Copyright © 2017-devYuSheng

---

